$(window).on("load",function(e){
  e.preventDefault();
  e.stopImmediatePropagation();
  $('body').on('show.bs.modal', function() {
    $('.modal-body').overlayScrollbars({
  	  className: "os-theme-round-dark",
      overflowBehavior: {
        x: 'hidden'
      },
      resize: "none",
      scrollbars: {
        autoHide: "never"
      }
  	});
    setTimeout(function(){
      $(".os-viewport.os-viewport-native-scrollbars-invisible").scrollTop(0,0);
    },200);
  });
  $('body').on('hidden.bs.modal',function(){
    $('#contenido').overlayScrollbars({
      className: "os-theme-round-dark",
      autoUpdate: true,
      nativeScrollbarsOverlaid : {
        showNativeScrollbars   : false,
        initialize             : true
      },
      overflowBehavior: {
        x: 'hidden',
        y : "scroll"
      },
      resize: "none",
      scrollbars: {
        autoHide: "never",
        touchSupport: true,
      }
    });
  });
});

function menuElegant(){
  $("#contenido").css("height",$(window).height()-80);
}

$("#enviarFormularioReserva").unbind("click").click(function(){
  var vacios = 0;
  $('#formularioIngreso').find('input').each(function(){
      if($(this).val() === ""){
        vacios++;
        $(this).addClass("is-invalid");
      }
  });
  $('#formularioIngreso').find('select').each(function(){
    if($(this).val() === "0"){
      vacios++;
      $(this).addClass("select2-error");
    }
  });
  if(vacios > 0){
    alertasToast("Debe ingresar la totalidad de campos para enviar la reserva","info");
  }
  else{
    if($("#celularFomularioIngreso").val().length < 9){
      alertasToast("Debe ingresar los 9 dígitos del móvil","info");
      $("#celularFomularioIngreso").addClass("is-invalid");
    }
    else{
      $("#bodyConfirmarReserva").html('<span style="font-size: 12pt; font-weight: bold;">¿Esta seguro de reservar la siguiente hora?</span><br><br><span>Fecha: ' + $("#fechaFomularioIngreso").val() + '</span><br><span>' + $('select[id="rangoFomularioIngreso"] option:selected').text() + '</span>');
      $("#modalConfirmarReserva").modal("show");
    }
  }
});

$("input[id*='FomularioIngreso']").unbind("click").on("input",function(){
  $(this).removeClass("is-invalid");
});

$("#rangoFomularioIngreso").unbind("click").on("input",function(){
  $(this).removeClass("select2-error");
});

function alertasToast(texto,tipo){
  toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": false,
    "progressBar": true,
    "positionClass": "toast-top-right",
    "preventDuplicates": false,
    "onclick": false,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "2000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "slideDown",
    "hideMethod": "slideUp"
  }
  if(tipo === "check"){
    toastr["success"](texto);
  }
  else if(tipo === "info"){
    toastr["info"](texto);
  }
  else if(tipo === "error"){
    toastr["error"](texto);
  }
  else{
    toastr["info"](texto);
  }
}

$('.input-number').on('input', function () {
    this.value = this.value.replace(/[^0-9]/g,'');
});

$("#dniFomularioIngreso").rut({
  formatOn: 'blur',
  minimumLength: 8,
  validateOn: 'change'
}).on('rutInvalido', function(e) {
  if($("#dniFomularioIngreso").val() !== ''){
    var random = Math.round(Math.random() * (1000000 - 1) + 1);
    alertasToast("El DNI ingresado no es válido","info");
    $("#dniFomularioIngreso").val("");
    $("#dniFomularioIngreso").addClass("is-invalid");
  }
});

$('.input-email').on('blur', function () {
  if($(this).val().indexOf('@', 0) == -1 || $(this).val().indexOf('.', 0) == -1) {
    if($(this).val() !== ""){
      alertasToast("El correo electrónico introducido no es correcto","info");
      $(this).val("");
      $(this).addClass("is-invalid");
    }
  }
});

$("#confirmarConfirmarReserva").unbind("click").click(function(){
  $("#modalConfirmarReserva").modal("hide");
  $("#modalAlertasSplash").modal({backdrop: 'static', keyboard: false});
  $("#textoModalSplash").html("<img src='view/img/loading.gif' class='splash_charge_logo'><font style='font-size: 12pt;'>Cargando</font>");
  $('#modalAlertasSplash').modal('show');
  var parametros = {};

  $('#formularioIngreso').find('input').each(function(){
    if($(this).attr("id") === "dniFomularioIngreso"){
      parametros[$(this).attr('id')] = ($(this).val().replace(".","").replace(".",""));
    }
    else{
      parametros[$(this).attr('id')] = ($(this).val());
    }
  });
  $('#formularioIngreso').find('select').each(function(){
    parametros[$(this).attr('id')] = ($(this).val());
  });

  parametros['rangoEmail'] = $('select[id="rangoFomularioIngreso"] option:selected').text().split('(Disponible')[0];

  $.ajax({
    url:   'controller/datosChequeoDNI.php',
    type:  'post',
    data:  parametros,
    success: function (response2) {
      var p = jQuery.parseJSON(response2);
      if(p.aaData.length !== 0){
        if(p.aaData[0].CANTIDAD === "0"){
          $.ajax({
            url: "controller/ingresarReservaRango.php",
            type: 'POST',
            data: parametros,
            success:  function (response) {
              var p = response.split(",");
              if(response.localeCompare("Sin datos")!= 0 && response != ""){
                if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
                  $.ajax({
                      url:   'controller/datosPisoRango.php',
                      type:  'post',
                      success: async function (response2) {
                        var p = jQuery.parseJSON(response2);
                        if(p.aaData.length !== 0){
                          setTimeout(function(){
                            alertasToast("Reserva ingresada correctamente","check");
                          },500);

                          var parametros_sms = {
                            "celular": $("#celularFomularioIngreso").val(),
                            "mensaje": 'Estimado ' + $("#nombreFomularioIngreso").val() + ' se ingreso una reserva de casino para el ' + $("#fechaFomularioIngreso").val() + ' según los siguientes datos ' + $('select[id="rangoFomularioIngreso"] option:selected').text().split('(Disponible')[0]
                          }

                          $.ajax({
                            url: "controller/envio_sms.php",
                            type: 'POST',
                            data: parametros_sms,
                            success:  function (response) {
                              setTimeout(function(){
                                $('#formularioIngreso').find('input').each(function(){
                                  $(this).val("");
                                  $(this).removeClass("is-invalid");
                                });

                                $("#fechaFomularioIngreso").val(moment().format('YYYY-MM-DD'))

                                $('#modalAlertasSplash').modal('hide');
                                alertasToast("SMS enviado correctamente","check");
                              },500);
                            }
                          });


                          var selRango = '';
                          for(var k = 0; k < p.aaData.length; k++){
                            if(p.aaData[k].DISPONIBLE > 0){
                              selRango += '<option value="' + p.aaData[k].IDRANGO + '">Piso: ' + p.aaData[k].PISO + ' - Rango: ' + p.aaData[k].RANGO + ' (Disponible: ' + p.aaData[k].DISPONIBLE + ')</option>';
                            }
                          }
                          $("#rangoFomularioIngreso").html(selRango);

                          if( !/AppMovil|Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                            $("#rangoFomularioIngreso").select2('destroy').select2({
                                theme: 'bootstrap4', width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style', placeholder: $(this).data('placeholder'), allowClear: Boolean($(this).data('allow-clear')), closeOnSelect: !$(this).attr('multiple')
                            });
                          }

                          for(var i = 0; i < p.aaData.length; i++){
                            var infoCorp = "<label style='font-size: 10pt; margin-bottom: 5pt; margin-left: 10pt;'>Ocupación</label><br>";
                            await $.ajax({
                              url:   'controller/datosPisoRangoDetalle.php',
                              type:  'post',
                              data: {
                                "idrango":  p.aaData[i].IDRANGO,
                                "piso": p.aaData[i].PISO
                              },
                              success: async function (response3) {
                                var p3 = jQuery.parseJSON(response3);
                                for(var j = 0; j < parseInt(p.aaData[i].RESERVADO); j++){
                                  infoCorp += "<span class='fas fa-user-alt'style='margin: 6pt; color: red;' title='" + p3.aaData[j].NOMBRE + "\n" + p3.aaData[j].DNI + "\n" + p3.aaData[j].MAIL + "'></span>";
                                }
                                for(var j = 0; j < parseInt(p.aaData[i].DISPONIBLE); j++){
                                  infoCorp += "<span class='fas fa-user-alt'style='margin: 6pt; color: green;' title='Disponible'></span>";
                                }
                                $("#cuerpoDatosInformativos" + (i+1)).html(infoCorp);
                              }
                            });
                          }
                        }
                      }
                    });
                }
                else{
                  $('#modalAlertasSplash').modal('hide');
                  alertasToast("Error al ingresar la reserva, favor comunicarlo a soporte","error");
                }
              }
              else{
                $('#modalAlertasSplash').modal('hide');
                alertasToast("Error al ingresar la reserva, favor comunicarlo a soporte","error");
              }
            }
          });
        }
        else{
          $('#modalAlertasSplash').modal('hide');
          alertasToast("El DNI ingresado ya posee una reserva","info");
        }
      }
      else{
        $('#modalAlertasSplash').modal('hide');
        alertasToast("Error al chequear el rut a ingresar reserva","info");
      }
    }
  });
});
