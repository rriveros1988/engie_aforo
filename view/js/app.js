var lineaTiempo = '';
var personalPropio = '';
var app = angular.module("WPApp", ["ngRoute"]);

app.config(function($routeProvider, $locationProvider) {
    $routeProvider
    .when("/home", {
        controller: "homeController",
        controllerAs: "vm",
        templateUrl : "view/home/home.html?idLoad=74"
    })
    .otherwise({redirectTo: '/home'});

    $locationProvider.hashPrefix('');
});

app.controller("homeController", function(){
  setTimeout(async function(){
    $("#modalAlertasSplash").modal({backdrop: 'static', keyboard: false});
    $("#textoModalSplash").html("<img src='view/img/loading.gif' class='splash_charge_logo'><font style='font-size: 12pt;'>Cargando</font>");
    $('#modalAlertasSplash').modal('show');
      $('#contenido').show();
      $('#menu-lateral').show();
      $('#footer').parent().show();
      $('#footer').show();
      $('#header').parent().show();
      $('#header').show();

      var min = new Date();
      $("#fechaFomularioIngreso").datepicker({
        dateFormat: "yy-mm-dd",
        changeMonth: true,
        changeYear: true,
        minDate: min,
        maxDate: min,
        yearRange: '1920:2040',
        firstDay: 1,
        changeMonth: true,
        changeYear: true,
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá']
      });

      $("#fechaFomularioIngreso").val(moment().format('YYYY-MM-DD'));

      var tam = ($(window).height() - 110);
      if( !/AppMovil|Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        var cuadro = (tam-45)/4;
      }
      else{
        var cuadro = (tam)/4;
      }
      var cuadroTitulo = cuadro*0.12;
      var cuadroCuerpo = cuadro*0.88;

      setTimeout(function(){
        $("#datosInformativos").css("height",tam);

        $.ajax({
          url:   'controller/datosPisoRango.php',
          type:  'post',
          success: async function (response2) {
            var p = jQuery.parseJSON(response2);
            if(p.aaData.length !== 0){
              var selRango = '';
              for(var k = 0; k < p.aaData.length; k++){
                if(p.aaData[k].DISPONIBLE > 0){
                  selRango += '<option value="' + p.aaData[k].IDRANGO + '">Piso: ' + p.aaData[k].PISO + ' - Rango: ' + p.aaData[k].RANGO + ' (Disponible: ' + p.aaData[k].DISPONIBLE + ')</option>';
                }
              }
              $("#rangoFomularioIngreso").html(selRango);

              if( !/AppMovil|Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                $("#rangoFomularioIngreso").select2({
                    theme: 'bootstrap4', width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style', placeholder: $(this).data('placeholder'), allowClear: Boolean($(this).data('allow-clear')), closeOnSelect: !$(this).attr('multiple')
                });
              }
              $("#rowDatosInformativos").append('<div style="margin-bottom: 10pt; text-align: left;" class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12"><font style="font-weight: bold; font-size: 17pt;"><span style="color: #626262;" class="fas fa-users">&nbsp;&nbsp;Aforo actual</span></font></div>');
              for(var i = 0; i < p.aaData.length; i++){
                $("#rowDatosInformativos").append('<div id="cuadroDatosInformativos' + (i+1) + '" style="text-align: left; height: ' + cuadro + 'px;" class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-xs-12"></div>');
                if( !/AppMovil|Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                  $("#cuadroDatosInformativos" + (i+1)).append('<div class="row"><div id="tituloDatosInformativos' + (i+1) + '" style="text-align: left; border: 1px solid #e2e2e2; height: ' + cuadroTitulo + 'px; background-color: rgba(9, 71, 50, 1.0); color: white; text-align: center; font-size: 10pt; vertical-align: middle; font-weight: bold;" class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12"></div><div id="cuerpoDatosInformativos' + (i+1) + '" style="text-align: left; border: 1px solid #e2e2e2; height: ' + cuadroCuerpo + 'px; font-weight: bold; color: gray; font-size: 20pt;" class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12"></div></div>');
                }
                else{
                  $("#cuadroDatosInformativos" + (i+1)).append('<div class="row"><div id="tituloDatosInformativos' + (i+1) + '" style="text-align: left; border: 1px solid #e2e2e2; height: ' + cuadroTitulo + 'px; background-color: rgba(9, 71, 50, 1.0); color: white; text-align: center; font-size: 8pt; vertical-align: middle; font-weight: bold;" class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12"></div><div id="cuerpoDatosInformativos' + (i+1) + '" style="text-align: left; border: 1px solid #e2e2e2; height: ' + cuadroCuerpo + 'px; font-weight: bold; color: gray; font-size: 20pt;" class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12"></div></div>');
                }
                $("#tituloDatosInformativos" + (i+1)).append("<label>Piso: " + p.aaData[i].PISO + " - Rango: " + p.aaData[i].RANGO + "</label>");
                var infoCorp = "<label style='font-size: 10pt; margin-bottom: 0pt; margin-left: 10pt;'>Ocupación</label><br>";
                await $.ajax({
                  url:   'controller/datosPisoRangoDetalle.php',
                  type:  'post',
                  data: {
                    "idrango":  p.aaData[i].IDRANGO,
                    "piso": p.aaData[i].PISO
                  },
                  success: async function (response3) {
                    var p3 = jQuery.parseJSON(response3);
                    for(var j = 0; j < parseInt(p.aaData[i].RESERVADO); j++){
                      infoCorp += "<span class='fas fa-user-alt'style='margin: 6pt; color: red;' title='" + p3.aaData[j].NOMBRE + "\n" + p3.aaData[j].DNI + "\n" + p3.aaData[j].MAIL + "'></span>";
                    }
                    for(var j = 0; j < parseInt(p.aaData[i].DISPONIBLE); j++){
                      infoCorp += "<span class='fas fa-user-alt'style='margin: 6pt; color: green;' title='Disponible'></span>";
                    }
                    $("#cuerpoDatosInformativos" + (i+1)).html(infoCorp);
                  }
                });
              }
            }
          }
        });

        var intervaloConsultas = setInterval(function(){
          alertasToast("Actualizando aforo","info");
          var sel = $("#rangoFomularioIngreso").val();
          $.ajax({
            url:   'controller/datosPisoRango.php',
            type:  'post',
            success: async function (response2) {
              var p = jQuery.parseJSON(response2);
              if(p.aaData.length !== 0){
                var selRango = '';
                for(var k = 0; k < p.aaData.length; k++){
                  if(p.aaData[k].DISPONIBLE > 0){
                    if(p.aaData[k].IDRANGO == sel){
                        selRango += '<option selected value="' + p.aaData[k].IDRANGO + '">Piso: ' + p.aaData[k].PISO + ' - Rango: ' + p.aaData[k].RANGO + ' (Disponible: ' + p.aaData[k].DISPONIBLE + ')</option>';
                    }
                    else{
                        selRango += '<option value="' + p.aaData[k].IDRANGO + '">Piso: ' + p.aaData[k].PISO + ' - Rango: ' + p.aaData[k].RANGO + ' (Disponible: ' + p.aaData[k].DISPONIBLE + ')</option>';
                    }
                  }
                }
                $("#rangoFomularioIngreso").html(selRango);

                if( !/AppMovil|Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                  $("#rangoFomularioIngreso").select2('destroy').select2({
                      theme: 'bootstrap4', width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style', placeholder: $(this).data('placeholder'), allowClear: Boolean($(this).data('allow-clear')), closeOnSelect: !$(this).attr('multiple')
                  });
                }

                for(var i = 0; i < p.aaData.length; i++){
                  var infoCorp = "<label style='font-size: 10pt; margin-bottom: 5pt; margin-left: 10pt;'>Ocupación</label><br>";
                  await $.ajax({
                    url:   'controller/datosPisoRangoDetalle.php',
                    type:  'post',
                    data: {
                      "idrango":  p.aaData[i].IDRANGO,
                      "piso": p.aaData[i].PISO
                    },
                    success: async function (response3) {
                      var p3 = jQuery.parseJSON(response3);
                      for(var j = 0; j < parseInt(p.aaData[i].RESERVADO); j++){
                        infoCorp += "<span class='fas fa-user-alt'style='margin: 6pt; color: red;' title='" + p3.aaData[j].NOMBRE + "\n" + p3.aaData[j].DNI + "\n" + p3.aaData[j].MAIL + "'></span>";
                      }
                      for(var j = 0; j < parseInt(p.aaData[i].DISPONIBLE); j++){
                        infoCorp += "<span class='fas fa-user-alt'style='margin: 6pt; color: green;' title='Disponible'></span>";
                      }
                      $("#cuerpoDatosInformativos" + (i+1)).html(infoCorp);
                    }
                  });
                }
              }
            }
          });
        },30000);

        setTimeout(function(){
          $('#modalAlertasSplash').modal('hide');
        },2000);
        menuElegant();
      },100);
  },200);
});
